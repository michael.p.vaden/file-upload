const express = require("express");
const fileUpload = require("express-fileupload");
const cors = require("cors");

const uploads = require("./routes/uploads");

const app = express();
const port = 3001;
const clientPort = 3000;

app.use(
  cors({
    origin: `http://localhost:${clientPort}`,
    credentials: true,
  })
);
app.use(fileUpload());
app.use("/upload", uploads);

app.get("/", (req, res) => {
  res.send(`App running.`);
});

app.listen(port, () => {
  console.log(`App running on http://localhost:${port}`);
});
