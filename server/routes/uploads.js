const express = require("express");
let router = express.Router();

router.put("/", async (req, res) => {
  try {
    if (!req.files) {
      res.send({
        status: false,
        message: "No files found.",
      });
    } else {
      const files = req.files["file"];

      if (files.length) {
        files.forEach((file) => {
          file.mv(`./files/${file.name}`);
        });
        res.send({
          status: true,
          message: "Files uploaded.",
        });
      } else {
        files.mv(`./files/${files.name}`);
        res.send({
          status: true,
          message: "File is uploaded.",
        });
      }
    }
  } catch (e) {
    res.status(500).send(e);
  }
});

module.exports = router;
