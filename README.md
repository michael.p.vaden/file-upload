![image](https://gitlab.com/michael.p.vaden/file-upload/-/raw/master/server/files/example.jpg)

## file-upload

Drag and drop file uploader with a demo backend that stores uploaded files locally in "files" folder.

### To run

Open two terminals and navigate to file-upload/server in one terminal and file-upload/client in the other.

### Client

`npm i` to install dependencies.
`npm run start` to start client.

### Server

`npm i` to install dependencies.
`npm run start` to start server.
