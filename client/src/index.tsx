import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import FileUpload from "./lib/FileUpload/FileUpload";

ReactDOM.render(
  <React.StrictMode>
    <div id="demo-container">
      <FileUpload maxUploadFileSizeMB={1} />
    </div>
  </React.StrictMode>,
  document.getElementById("root"),
);
