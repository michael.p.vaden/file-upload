import React, { useEffect } from "react";
import "./FileUpload.css";

interface Props {
  refKey: number;
  name: string;
  size: number;
  progress: number;
  cancel: Function;
  status: string;
}

const FileUploadItem: React.FC<Props> = (props: Props) => {
  const totalFileSize = handleFileSize(props.size);
  let displayCancel = handleCancelDisplay();

  useEffect(() => {
    const progressBar = document.querySelectorAll(".upload-progress-bar")[props.refKey] as HTMLElement;
    setProgressBarWidth(progressBar);
    checkProgressComplete(progressBar);
    checkStatus(progressBar);
  });

  return (
    <div className="upload-item flex-w-100">
      <div className="upload-item-top flex-r-l">
        <div className="flex-r-l">
          <div className="upload-item-name">{props.name}</div>
        </div>
        <div className="flex-r-r">
          <div className="upload-progress">
            {props.status === "cancelled"
              ? "Cancelled"
              : props.progress < props.size
              ? `${handleFileSize(props.progress)} / ${totalFileSize}`
              : "Complete"}
          </div>
          {displayCancel}
        </div>
      </div>
      <div className="upload-progress-bar-empty">
        <div className="upload-progress-bar"></div>
      </div>
    </div>
  );

  function handleCancel() {
    props.cancel();
  }

  function handleCancelDisplay() {
    if (props.status === "cancelled" || props.progress >= props.size) return "";
    return (
      <div className="upload-cancel flex-r-c" onClick={handleCancel}>
        +
      </div>
    );
  }

  function checkStatus(progressBar: HTMLElement) {
    if (props.status === "cancelled") {
      progressBar.style.width = `100%`;
      progressBar.classList.add("upload-cancelled");
    }
  }

  function setProgressBarWidth(progressBar: HTMLElement) {
    progressBar.style.width = `${Math.floor((props.progress / props.size) * 100)}%`;
  }

  function checkProgressComplete(progressBar: HTMLElement) {
    if (props.progress >= props.size) {
      handleProgressComplete(progressBar);
    }
  }

  function handleProgressComplete(progressBar: HTMLElement) {
    progressBar.classList.add("upload-progress-complete");
  }

  function getFileSizeUnit(fileSize: number) {
    if (fileSize / 1000000 >= 1) return "MB";
    if (fileSize / 1000 >= 1) return "KB";
    return `B`;
  }

  function convertFileSizeToUnit(size: number, unitType: string) {
    if (unitType === "MB") return `${(size / 1000000).toFixed(2)}MB`;
    if (unitType === "KB") return `${(size / 1000).toFixed(2)}KB`;
    if (unitType === "B") return `${size}B`;
    return `0`;
  }

  function handleFileSize(fileSize: number) {
    const unitType = getFileSizeUnit(fileSize);
    return convertFileSizeToUnit(fileSize, unitType);
  }
};

export default FileUploadItem;
