import React, { useState } from "react";
import FileUploadItem from "./FileUploadItem";
import axios from "axios";
import { uploadAPI } from "./uploadAPI";
import "./FileUpload.css";

interface Props {
  maxUploadFileSizeMB: number;
}

const FileUpload: React.FC<Props> = (props: Props) => {
  interface Upload {
    key: number;
    name: string;
    size: number;
    progress: number;
    cancel: Function;
    status: string;
  }

  const [uploads, setUploads] = useState<Upload[]>([]);
  const [error, setError] = useState("");

  return (
    <div className="FileUpload flex-l">
      <div className="file-dropper flex-r-c flex-w-100">
        <label>Drag & drop</label>
        <p>
          your files here or <u>browse</u>
        </p>
        <input type="file" multiple={true} onChange={handleFileUploads}></input>
      </div>
      {error !== "" ? <p className="upload-error">{error}</p> : null}
      <div className="file-upload-list-wrapper">
        <div className="file-upload-list">
          {uploads.map((uploadItem) => {
            return (
              <FileUploadItem
                key={uploadItem.key}
                refKey={uploadItem.key}
                name={uploadItem.name}
                size={uploadItem.size}
                progress={uploadItem.progress}
                cancel={uploadItem.cancel}
                status={uploadItem.status}
              />
            );
          })}
        </div>
      </div>
    </div>
  );

  function handleFileUploads(e: React.ChangeEvent<HTMLInputElement>) {
    const files = e.target.files;
    let keyModifier = 0;

    if (files) {
      for (
        let uploadsKey = uploads.length;
        uploadsKey < uploads.length + files.length;
        uploadsKey++
      ) {
        let fileKey = uploadsKey - uploads.length;
        console.log(uploadsKey, (keyModifier), uploads.length);
        if (files[fileKey].size <= maxFileSizeToBytes()) {
          let upload: Upload = {
            key: uploadsKey + keyModifier,
            name: files![fileKey].name,
            size: files![fileKey].size,
            progress: 0,
            cancel: () => {},
            status: "normal",
          };
          requestUpload(files[fileKey], upload);
          addUpload(upload);
        } else {
          console.log("fileKey" + fileKey);
          keyModifier = keyModifier - 1;
          displayErrorMessage(
            `File ${
              files[fileKey].name
            } is too large (${props.maxUploadFileSizeMB}MB max).`,
          );
          setTimeout(resetErrorMessage, 5000);
        }
      }
    }
  }

  function maxFileSizeToBytes() {
    return props.maxUploadFileSizeMB * 1000000;
  }

  function displayErrorMessage(message: string) {
    setError((prevMessage) => {
      return prevMessage + message;
    });
  }

  function resetErrorMessage() {
    setError(() => {
      return "";
    });
  }

  function requestUpload(file: Blob, upload: Upload) {
    const CancelToken = axios.CancelToken;
    const formData = new FormData();
    formData.append("file", file);

    uploadAPI({
      method: "put",
      url: "./upload",
      data: formData,
      onUploadProgress: (e: ProgressEvent) => {
        upload.progress = e.loaded;
        updateUpload(upload);
      },
      cancelToken: new CancelToken(function executor(c) {
        upload.cancel = c;
      }),
    }).catch((e) => {
      upload.status = "cancelled";
      updateUpload(upload);
    });
  }

  function addUpload(upload: Upload) {
    setUploads((prevUploads: Upload[]) => {
      return [...prevUploads, upload];
    });
  }

  function updateUpload(upload: Upload) {
    setUploads((prevUploads: Upload[]) => {
      prevUploads[upload.key].progress = upload.progress;
      return [...prevUploads];
    });
  }
};

export default FileUpload;
